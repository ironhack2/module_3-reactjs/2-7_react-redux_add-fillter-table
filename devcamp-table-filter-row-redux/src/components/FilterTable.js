import * as React from 'react';
import { Container, Grid, TextField, Button } from '@mui/material';
import { Table, TableBody, TableContainer, TableHead, TableRow, TableCell, Paper } from '@mui/material';
import { styled } from '@mui/material';

import { useSelector, useDispatch } from "react-redux";


function createData(stt, drink) {
    return { stt, drink };
}

const rows = [
    createData(1, "Cà phê"),
    createData(2, "Trà tắc"),
    createData(3, "Pepsi"),
    createData(4, "Cocacola"),
    createData(5, "Trà sữa"),
    createData(6, "Matcha"),
    createData(7, "Hồng trà"),
    createData(8, "Trà xanh kem cheese"),
    createData(9, "Trà đá"),
];

var checkFilter = "";


function FilterTable() {

    const dispatch = useDispatch();

    const { taskInput } = useSelector((reduxData) => reduxData.taskReducer);


    const inputChangeHandler = (event) => {
        dispatch({
            type: "TASK_INPUT_CHANGE",
            value: event.target.value
        });
    }


    const btnFilter = () => {
        checkFilter = taskInput;
        dispatch({
            type: "BTN_FILTER",
            filter: checkFilter
        });
    }


    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    return (
        <Container>
            <Grid container mt={5} mb={5}>
                <Grid item xs={3}>
                    <h3>Nhập nội dung dòng</h3>
                </Grid>

                <Grid item xs={7}>
                    <TextField fullWidth label="Nội dung" value={taskInput} onChange={inputChangeHandler} />
                </Grid>

                <Grid item xs={2} style={{ textAlign: "center" }} mt={1}>
                    <Button variant="outlined" onClick={btnFilter}>Lọc</Button>
                </Grid>
            </Grid>


            <TableContainer component={Paper} style={{ marginTop: "100px" }}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow style={{ backgroundColor: "#795548" }}>
                            <TableCell style={{ color: "#ffffff", fontSize: "24px" }}> <b>STT</b></TableCell>
                            <TableCell align="left" style={{ color: "#ffffff", fontSize: "24px" }}> <b>Nội dung</b> </TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {(rows.filter(row => row.drink.toLowerCase().includes(checkFilter.toLowerCase()))).map((row, index) => {
                            return (
                                <StyledTableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}  >
                                    <TableCell component="th" scope="row"> {row.stt} </TableCell>
                                    <TableCell align="left">{row.drink}</TableCell>
                                </StyledTableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>

    )
}

export default FilterTable