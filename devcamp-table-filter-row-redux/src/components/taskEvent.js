const initialState = {
    taskInput: "",
    tableDrink: [],
    fillter: ""
}

const TaskEvent = (state = initialState, action) => {
    switch (action.type) {
        case "TASK_INPUT_CHANGE":
            return {
                ...state,
                taskInput: action.value
            }
        case "BTN_FILTER":
            return {
                ...state,
               filter: action.filter
            }

        default:
            return state;
    }
}

export default TaskEvent;