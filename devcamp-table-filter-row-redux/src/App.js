import FilterTable from "./components/FilterTable";

function App() {
  return (
    <div>
      <FilterTable />
    </div>
  );
}

export default App;
