import { Container, Grid, TextField, Button, MenuList, MenuItem } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";

function Task() {
    const dispatch = useDispatch();

    const { taskNameInput, taskList } = useSelector((reduxData) => reduxData.taskReducer);

    console.log(taskNameInput);
    console.log(taskList);

    const inputChangeHandler = (event) => {
        dispatch({
            type: "TASK_INPUT_CHANGE",
            value: event.target.value
        })
    }

    const addTaskClick = () => {
        dispatch({
            type: "ADD_TASK"
        })
    }

    const changeTaskStatus = (index) => {
        dispatch({
            type: "CHANGE_TASK_STATUS",
            index: index
        })
    }

    return (
        <Container>
            <Grid container mt={3}>
                <Grid xs={9} md={9} lg={9} sm={12} item>
                    <TextField fullWidth placeholder="Input Task name" variant="outlined" label="Input Task name" value={taskNameInput} onChange={inputChangeHandler} />
                </Grid>
                <Grid xs={3} md={3} lg={3} sm={12} item textAlign={"center"}>
                    <Button variant="contained" onClick={addTaskClick}>Add Task</Button>
                </Grid>
            </Grid>
            <MenuList>
                { taskList.map((task, index) => {
                    return <MenuItem key={index} style={{color: task.status ? 'green': 'red'}} onClick={() => changeTaskStatus(index)}>{task.taskName}</MenuItem>
                }) }
            </MenuList>
        </Container>
    )
}

export default Task;

