const initialState = {
  taskNameInput: "",
  taskList: []
}

const TaskEvent = ( state = initialState, action) => {
  switch (action.type) {
      case "TASK_INPUT_CHANGE":
          return {
              taskNameInput: action.value,
              taskList: state.taskList
          }
      case "ADD_TASK":
          return {
              taskNameInput: "",
              taskList: [ ...state.taskList, {
                  taskName: state.taskNameInput,
                  status: false
              }]
          }
      case "CHANGE_TASK_STATUS":


          state.taskList[action.index].status = !state.taskList[action.index].status

          return {
            ...state
          }

      default:
          return state;
  }
}

export default TaskEvent;

