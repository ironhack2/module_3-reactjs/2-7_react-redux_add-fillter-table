import { createStore, combineReducers } from "redux";
import TaskEvent from "../components/TaskEvent";


const appReducer = combineReducers({
         taskReducer: TaskEvent
});

const store = createStore(
    appReducer,
    undefined,
    undefined
    // {},
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store