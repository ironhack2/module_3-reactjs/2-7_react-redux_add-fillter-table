const initialState = {
    taskInput: "",
    taskList: []
}

const TaskEvent = (state = initialState, action) => {
    switch (action.type) {
        case "TASK_INPUT_CHANGE":
            return {
                taskInput: action.value,
                taskList: state.taskList
            }

        case "ADD_TABLE":
            return {
                taskInput: "",
                taskList: [...state.taskList, {
                    taskName: state.taskInput,
                }]
            }

        default:
            return state;
    }
}

export default TaskEvent